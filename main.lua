--Quad Ball - a retro space shooter for up to four players
--Copyright 2021 Eric Duhamel

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <https://www.gnu.org/licenses/>.
if not lutro then lutro = love REVO = true end
a, b, c, d = lutro.getVersion()
print('LÖVE Ver: '..a..'.'..b..'.'..c..' '..d)

function lutro.load()
  LEFT, TOP, RIGHT, BOTTOM = 0, 0, 320, 240
  BG_IMG = lutro.graphics.newImage('images/space02.png')
  GOALS = {
    {name = 'WEST', score = 5, x = -99, y = 70, w = 100, h = 100, xx = 25, yy = 120},
    {name = 'NORTH', score = 5, x = 110, y = -99, w = 100, h = 100, xx = 124, yy = 30},
    {name = 'EAST', score = 5, x = 312, y = 70, w = 100, h = 100, xx = 225, yy = 120},
    {name = 'SOUTH', score = 5, x = 110, y = 232, w = 100, h = 100, xx = 124, yy = 205}}
  FONT = lutro.graphics.newImageFont("images/font032095.png",
    ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_')
  BACKDROP = get_backdrop(BG_IMG, GOALS, FONT)
  BOOM_IMG = lutro.graphics.newImage('images/boom03.png')
  MISS_IMG = lutro.graphics.newImage('images/miss03.png')
  PLAY_IMG = {lutro.graphics.newImage('images/ship00.png'),
              lutro.graphics.newImage('images/ship02.png'),
              lutro.graphics.newImage('images/ship04.png'),
              lutro.graphics.newImage('images/ship09.png')}
  BALL = {img=lutro.graphics.newImage('images/ball01.png'),
          qua=lutro.graphics.newQuad(0, 0, 32, 32, 64, 96),
          iw = 32, ih = 32, ox = -16, oy = -16, rad = 12,
          x = 160, y = 120, dx = 0, dy = 0, turn = 0, head = 0}
  PLAYERS = {get_player(1, 50, 120), get_player(2, 160, 200),
             get_player(3, 270, 120), get_player(4, 160, 40)}
  BALLS, EFFECTS, MISSILES = {BALL}, {}, {}
  BOOM00 = lutro.audio.newSource('sounds/sfx11.wav', 'static')
  BOOM01 = lutro.audio.newSource('sounds/sfx30.wav', 'static')
  FIRE00 = lutro.audio.newSource('sounds/sfx16.wav', 'static')
  GOAL00 = lutro.audio.newSource('sounds/sfx06.wav', 'static')
  FRAME = 0
end

function lutro.update(dt)
  FRAME = (FRAME+dt*1000)%256
  OBJECTS = {}
  SHOOTME = {}
  for i,obj in ipairs(PLAYERS) do
    control_shooters(i, obj, dt)
    control_thrusters(i, obj, dt)
    control_turners(i, obj, dt)
    obj_drift(obj, dt)
    obj_drag(obj, dt)
    obj_wrap(obj, LEFT, TOP, RIGHT, BOTTOM)
    player_animate(obj)
    table.insert(OBJECTS, obj)
    table.insert(SHOOTME, obj)
  end
  for i,obj in ipairs(BALLS) do
    obj_drift(obj, dt)
    obj_drag(obj, dt)
    local goal, gx, gy = obj_wrap(obj, LEFT, TOP, RIGHT, BOTTOM)
    if obj.kick and obj.kick > 0 then
      obj.qua:setViewport(32, 0, 32, 32)
      local goal = GOALS[goal]
      if goal and gx > goal.x and gx < goal.x+goal.w
         and gy > goal.y and gy < goal.y+goal.h then
        goal.score = goal.score-1
        GOAL00:play()
        if goal.score < 1 then WINNER, obj.kick = true, 0 end
        BACKDROP = get_backdrop(BG_IMG, GOALS, FONT)
      end
      obj.kick = obj.kick-(dt*60)
    else
      obj.kick = 0
      obj.qua:setViewport(0, 32, 32, 32)
    end
    table.insert(OBJECTS, obj)
    table.insert(SHOOTME, obj)
  end
  for i,obj in ipairs(MISSILES) do
    local armed, detonate = 250, 1200
    obj_drift(obj, dt)
    obj_wrap(obj, LEFT, TOP, RIGHT, BOTTOM)
    if obj.fuse < detonate then obj.fuse = obj.fuse + dt*1000 end
    for j,tar in ipairs(SHOOTME) do
      if compute_distance(obj, tar) < tar.rad and obj.fuse > armed then
          obj_bounce(obj, tar)
          if not WINNER then tar.kick = 150 end
          table.insert(EFFECTS, get_boom(obj.x, obj.y))
          BOOM01:play()
          obj.dead = true
      end
    end
    if not obj.dead and obj.fuse >= detonate then
      table.insert(EFFECTS, get_boom(obj.x, obj.y))
      BOOM00:play()
      obj.dead = true
    end
    table.insert(OBJECTS, obj)
  end
  for i=#EFFECTS,1,-1 do obj = EFFECTS[i]
    if not effect_animate(obj, dt) then table.remove(EFFECTS, i) end
  end
  for i=#MISSILES,1,-1 do obj = MISSILES[i]
    if obj.dead then table.remove(MISSILES, i) end
  end
end

function lutro.draw()
  lutro.graphics.scale(2)
  lutro.graphics.setFont(FONT)
  lutro.graphics.draw(BACKDROP, 0, 0)
  local n, m, l = 1, #OBJECTS, 1  -- shuffle draw order
  if math.floor(FRAME%2) == 0 then n, m, l = #OBJECTS,1,-1 end
  for i=n,m,l do obj_draw(OBJECTS[i]) end
  for _,obj in ipairs(EFFECTS) do obj_draw(obj) end
end

function apply_force(obj, dx, dy)
  obj.dx, obj.dy = obj.dx+dx, obj.dy+dy
end

function compute_angle(ob1, ob2)
  local x1, y1, x2, y2 = ob1.x, ob1.y, ob2.x, ob2.y
  return math.atan2(y1 - y2, x1 - x2)
end

function compute_distance(ob1, ob2)
  local x1, y1, x2, y2 = ob1.x, ob1.y, ob2.x, ob2.y
  local dx, dy = x2 - x1, y2 - y1
  return math.sqrt(dx*dx + dy*dy)
end

function compute_vector(angle, length)
  length = length or 1
  local dx = math.cos(angle)*length
  local dy = math.sin(angle)*length
  return dy, dx
end

function control_shooters(i, obj, dt)
  local launch_speed = 100
  if obj.cool > 0 then obj.cool = obj.cool - (dt*1000) else obj.cool = 0 end
  if player_trig(i, 2) and obj.cool == 0 then
    local dx, dy = compute_vector(obj.head, launch_speed)
    local lx, ly = compute_vector((obj.head)%6.28, 8)
    local rx, ry = compute_vector((obj.head+3.14)%6.28, 8)
    local lox, loy, rox, roy = obj.x-lx, obj.y-ly, obj.x-rx, obj.y-ry
    l_miss, r_miss = get_missile(lox, loy), get_missile(rox, roy)
    apply_force(l_miss, obj.dx+dy, obj.dy-dx)
    apply_force(r_miss, obj.dx+dy, obj.dy-dx)
    table.insert(MISSILES, l_miss)
    table.insert(MISSILES, r_miss)
    FIRE00:play()
    obj.cool = 500
  end
  if player_trig(i, 6) then
    obj.tag = i == 1 and 'WEST' or i == 2 and 'SOUTH'
           or i == 3 and 'EAST' or 'NORTH'
  elseif player_trig(i, 4) and WINNER then WINNER = false lutro.load()
  else obj.tag = false
  end
end

function control_thrusters(i, obj, dt)
  local force = 100
  if player_trig(i, 1) or player_trig(i, 5) then
    local dx, dy = compute_vector(obj.head, force*dt)
    apply_force(obj, dy, -dx)
  end
end

function control_turners(i, obj, dt)
  local radians = 5  -- rotation speed per second
  if player_trig(i, 7) then
    obj.turn = (obj.turn+(radians*dt))%6.28
  elseif player_trig(i, 8) then
    obj.turn = (obj.turn-(radians*dt))%6.28
  end
  obj.head = math.floor(obj.turn/(6.28/24))*(6.28/24)
end

function effect_animate(obj, dt)
  obj.a = obj.a + dt*1000
  if obj.a > 50 then
    obj.f = obj.f + 1
    if obj.f < 8 then
      obj.qua:setViewport((obj.f*obj.iw), 0, obj.iw, obj.ih)
    else
      return false
    end
    obj.a = 0
  end
  return true
end

function get_backdrop(image, goals, font)
  local canvas = lutro.graphics.newCanvas(320, 240)
  if lutro.graphics.reset then lutro.graphics.reset() end
  lutro.graphics.setCanvas(canvas)
  lutro.graphics.draw(image, 0, 0)
  lutro.graphics.setFont(font)
  if WINNER then set_winners(goals) end
  for i,goal in ipairs(goals) do
    lutro.graphics.print(goal.score, goal.x, goal.y)
    lutro.graphics.print(goal.score, goal.x+goal.w, goal.y)
    lutro.graphics.print(goal.score, goal.x, goal.y+goal.h)
    lutro.graphics.print(goal.score, goal.x+goal.w, goal.y+goal.h)
    if goal.win then
      lutro.graphics.print(goal.name..' WINS', goal.xx, goal.yy)
    end
  end
  lutro.graphics.setCanvas()
  return canvas
end

function get_boom(x, y)
  return {
    img = BOOM_IMG,
    qua = lutro.graphics.newQuad(0, 0, 8, 8, 64, 8),
    iw = 8, ih = 8, ox = -4, oy = -4,
    x = x, y = y, dx = dx, dy = dy, rad = 4,
    a = 0, f = 0}
end

function get_missile(x, y)
  return {
    img = MISS_IMG,
    qua = lutro.graphics.newQuad(0, 0, 8, 8, 8, 8),
    iw = 8, ih = 8, ox = -4, oy = -4,
    x = x, y = y, dx = 0, dy = 0, turn = 0, rad = 4,
    fuse = 0}
end

function get_player(n, x, y)
  local east, north, west, south = 0, 1.57, 3.14, 4.71
  local a = n == 1 and east or n == 2 and north or n == 3 and west or south
  return {
    img = PLAY_IMG[n], qua = lutro.graphics.newQuad(0, 0, 32, 32, 768, 32),
    iw = 32, ih = 32, ox = -16, oy = -16, head = 0,
    x = x, y = y, dx = 0, dy = 0, turn = a, rad = 16,
    cool = 500}
end

function set_winners(goals)
  -- set 'win' to true for each one with the highest 'score' number
  local hiscore = 0
  for _,goal in ipairs(goals) do
    if type(goal.score) == 'number' and goal.score > hiscore then
      hiscore = goal.score
    end
  end
  for _,goal in ipairs(goals) do
    if type(goal.score) == 'number' and goal.score == hiscore then
      goal.win = true
    end
  end
end

function obj_bounce(obj1, obj2)
  local angle = compute_angle(obj1, obj2)
  local fy, fx = compute_vector(angle, 50)
  apply_force(obj2, -fx, -fy)
end

function obj_drag(obj, dt)
  if obj.x > 0 and obj.y > 0 and obj.x < 320 and obj.y < 240 then
    local a = math.atan2(obj.dy, obj.dx)
    local d = (math.sqrt(obj.dx * obj.dx + obj.dy * obj.dy)/2)*dt
    local dx, dy = math.cos(a)*d, math.sin(a)*d
    obj.dx, obj.dy = obj.dx-dx, obj.dy-dy
  end  -- let object drift until in bounds
end

function obj_draw(obj)
--lutro.graphics.circle('line', obj.x, obj.y, obj.rad)
--lutro.graphics.rectangle('line', obj.x+obj.ox, obj.y+obj.oy, obj.iw, obj.ih)
  if obj.tag then lutro.graphics.print(obj.tag, obj.x, obj.y+16) end
  if REVO then ox, oy = -obj.ox, -obj.oy else ox, oy = obj.ox, obj.oy end
  local x, y = math.floor(obj.x), math.floor(obj.y)
  lutro.graphics.draw(obj.img, obj.qua, x, y, 0, 1, 1, ox, oy)
end

function obj_drift(obj, dt)
  obj.x, obj.y = obj.x+(obj.dx*dt), obj.y+(obj.dy*dt)
end

function obj_wrap(obj, left, top, right, bottom)
  local x, y = obj.x, obj.y
  if obj.x-obj.rad > right then obj.x = left-obj.rad return 3, x, y
  elseif obj.x+obj.rad < left then obj.x = right+obj.rad return 1, x, y
  elseif obj.y-obj.rad > bottom then obj.y = top-obj.rad return 4, x, y
  elseif obj.y+obj.rad < top then obj.y = bottom+obj.rad return 2, x, y
  else return false end
end

function player_animate(obj)
  local f = obj.head/(6.28/24)  -- only rotate to a matching animation frame
  obj.qua:setViewport(f*obj.iw, 0, obj.ih, obj.iw)
end

function player_trig(n, b)  -- get player intput (retropad or keyboard)
  local keys = {{ 'up','rctrl', 'tab', 'return', 'up','down','left','right'},
                {  'w','lctrl', 'tab', 'return',  'w',   's',   'a',    'd'},
                {  'i','space', 'tab', 'return',  'i',   'k',   'j',    'l'},
                {'kp8',  'kp0', 'tab', 'return','kp8', 'kp5', 'kp4',  'kp6'}}
  if not lutro.joystick.isDown then
    if n > 0 and n <= #keys then return lutro.keyboard.isDown(keys[n][b]) end
  else return lutro.joystick.isDown(n, b) end
end
