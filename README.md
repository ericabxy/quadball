# Quad Ball
a retro space shooter for four players, Quad Ball is designed akin to
video games on old Atari consoles: you are given zero instructions and
dropped directly into the action. Effort is being made to telegraph
game mechanics and add game modes in future updates.

## Gameplay
Quad Ball is played between four visually distinct space ships with no
color recognition needed. Press [left] and [right] on the RetroPad to
turn your ship. Hold [B] to activate thrusters. Use your blasters [Y]
to push the ball across an enemy goal line. Defend your goal against
the ball or you will lose a point. Once a player reaches 0 points, the
players with the most points left win the round.

## About
This game was sketched out during a two-day bout of creative mania 
suffered by the author. Quad Ball demonstrates what RetroArch and the 
Lutro core are capable of with just a few hundred lines of open-source 
code and public assets.

## RetroArch, Lutro, and LÖVE
Quad Ball is made for RetroArch, utilizing the LibRetro Lutro core. You 
can download 
[RetroArch](https://www.retroarch.com/index.php?page=platforms) and 
[Lutro](https://github.com/libretro/libretro-lutro) for free. In case 
you have to use the command line, Quad Ball can be run from within the 
source directory.

* $ retroarch -L path/to/lutro_libretro.so .

Quad Ball can also be played with [LÖVE](https://love2d.org/). Just 
start from the source code directory.

* $ love .

Two players are supported in Quad ball for LÖVE via the keyboard, in 
case you want to cram that many people around one. The controls are 
listed below. Press [Enter] to restart from game over.

* Player West
** Thrusters: Up
** Blasters: R-Ctrl
** Turners: Left and Right
** Tag: Down

* Player South
** Thrusters: W
** Blasters: L-Ctrl
** Turners: A and D
** Tag: S

* Player East
** Thrusters: I
** Blasters: Spacebar
** Turners: J and L
** Tag: K

* Player North
** Thrusters: Keypad 8
** Blasters: Keypad 0
** Turners: Keypad 4 and Keypad 6
** Tag: Keypad 5

------====----====----====----====----====----====----====----====------
