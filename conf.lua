function love.conf(t)
  t.version = '11.1'
  if t.window then
    t.window.title = 'Quad Ball'
    t.window.width = 640
    t.window.height = 480
  end
end
